from butler import Butler
from methods import Methods


class Tests:
    def __init__(self):
        self._butler = Butler()

        self._butler.subscribe_method(Methods.Profile.RequestCaptcha, self.request_captcha)

        self._butler.send_rpc(
            Methods.Profile.LoginWithPassword,
            {
                "username": "freakrho@gmail.com",
                "password": "Ka61%C7OKoSr"
            },
            Tests.receive_msg
        )

    def request_captcha(self, data):
        url = data["recaptchaUrl"]
        print("Request captcha " + str(data))

    def send_rpc(self, method: str, params: dict, callback):
        self._butler.send_rpc(method, params, callback)

    @staticmethod
    def receive_msg(data: dict):
        print("Received " + str(data))

    def receive_profile(self, data: dict):
        self._profile = data["profile"]["user"]
        print(self._profile)
        self.send_rpc(Methods.Fetch.ProfileOwnedKeys, {
            "profileId": self._profile["id"],
            "fresh": True
        }, Tests.receive_msg)
