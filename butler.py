import logging
import threading
import socket
import json
import os
import subprocess
from datetime import datetime
from typing import Callable, Optional

from .methods import Methods

NOTIFICATION = "butlerd/listen-notification"
ADDRESS = "address"
TCP = "tcp"
SECRET = "secret"
BUFFER_SIZE = 1024 * 10
METHOD = "method"
LOG = "Log"
ID = "id"
RESULT = "result"
ERROR = "error"
BUFFER_TIME_TO_LIVE = 20  # seconds
DB_FILE = "butler.db"
if os.name == 'nt':
    DB_DIR = os.path.expandvars(r'%APPDATA%\itch\db')
else:
    DB_DIR = os.path.abspath(__file__)
logger = logging.getLogger(__name__)


class Butler:
    def __init__(self):
        self.initialized = False
        self._callbacks = {}

        params = ["butler", "daemon", "--json", "--destiny-pid", str(os.getpid()),
                  "--dbpath", os.path.join(DB_DIR, DB_FILE)]
        self._process = subprocess.Popen(params, stdout=subprocess.PIPE)
        self._address = ("0.0.0.0", 0)
        self._secret = ""
        for line in iter(self._process.stdout.readline, ''):
            data = json.loads(line.rstrip())
            if "type" in data and data["type"] == NOTIFICATION:
                address = data[TCP][ADDRESS]
                listed_address = address.split(':')
                self._address = (listed_address[0], int(listed_address[1]))
                self._secret = data[SECRET]
                break
        logger.info("Address: " + str(self._address))
        logger.info("Secret: " + self._secret)
        self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._socket.connect(self._address)
        self._id = 0

        self._update_thread = threading.Thread(target=self.update)
        self._update_thread.start()
        self._buffer = ""
        self._buffer_start_time = None
        self._subscribed_methods = {}

        self.send_rpc(Methods.Meta.Authenticate, {}, self.receive_authentication_response)

    def subscribe_method(self, method: str, callback: Callable[[dict], None]):
        if method not in self._subscribed_methods:
            self._subscribed_methods[method] = []
        self._subscribed_methods[method].append(callback)

    def receive_authentication_response(self, params: dict):
        if "ok" in params:
            if params["ok"]:
                self.initialized = True
                logger.debug("Initialized OK")

    def _process_msg(self, data: dict):
        if data == {}:
            return

        if ID in data:
            result_data = {}
            msg_id = data[ID]
            if RESULT in data:
                result_data = data[RESULT]
            if ERROR in data:
                logger.error(data[ERROR])
                result_data = data

            if msg_id in self._callbacks:
                callback = self._callbacks.pop(data[ID])
                callback(result_data)

        if METHOD in data:
            method = data[METHOD]
            if method == LOG:
                params = data["params"]
                level = logging._nameToLevel[str(params["level"]).upper()]
                logger.log(level, params["message"])
            else:
                logger.debug("Got method: " + str(data))
                if method in self._subscribed_methods:
                    for callback in self._subscribed_methods[method]:
                        callback(data)
        # else:
        #     logger.info("Got message " + str(data))

    def update(self):
        while True:
            # wait for response
            msg = self._socket.recv(BUFFER_SIZE)
            if self._buffer and (datetime.now() - self._buffer_start_time).total_seconds() > BUFFER_TIME_TO_LIVE:
                logger.info("Discarding buffer " + self._buffer)
                self._buffer = ""
            lines = msg.decode().split('\n')
            for line in lines:
                line = line.strip()
                print(line)
                if line:
                    try:
                        data = json.loads(line)
                    except json.decoder.JSONDecodeError:
                        if not self._buffer:
                            self._buffer_start_time = datetime.now()
                        self._buffer += line
                        data = {}
                    self._process_msg(data)
            if self._buffer:
                try:
                    data = json.loads(self._buffer)
                    self._process_msg(data)
                    self._buffer = ""
                except json.decoder.JSONDecodeError:
                    pass

    def get_id(self):
        self._id += 1
        return self._id

    def send(self, message: dict):
        msg = json.dumps(message) + "\n"
        print("Sending message: " + msg)
        self._socket.send(msg.encode())

    def send_rpc(self, method: str, params: dict, callback: Optional[Callable[[dict], None]]):
        params["secret"] = self._secret
        # get a unique id
        msg_id = str(self.get_id())
        # construct the message
        msg = {
            "jsonrpc": "2.0",
            "method": method,
            "params": params,
            "id": msg_id
        }
        # send the message
        if callback is not None:
            self._callbacks[msg_id] = callback
        self.send(msg)

    def respond(self, message_id: int, data: dict):
        self.send({
          "jsonrpc": "2.0",
          "id": message_id,
          "result": data,
        })

    def login_with_api_key(self, key: str, callback):
        self.send_rpc(Methods.Profile.LoginWithAPIKey, {"apiKey": key}, callback)
