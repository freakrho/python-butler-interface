# Methos available on butler rpc (added a bunch, from now on will add when needed)
class Methods(object):
    InstallVersionSwitchPick = "InstallVersionSwitchPick"
    PickUpload = "PickUpload"
    Progress = "Progress"

    class Meta:
        META = "Meta."
        Authenticate = META + "Authenticate"
        Flow = META + "Flow"
        Shutdown = META + "Shutdown"

    class Version:
        VERSION = "Version."
        Get = VERSION + "Get"

    class Network:
        NETWORK = "Network."
        SetSimulateOffline = NETWORK + "SetSimulateOffline"
        SetBandwidthThrottle = NETWORK + "SetBandwidthThrottle"

    class Profile:
        PROFILE = "Profile."
        List = PROFILE + "List"
        LoginWithPassword = PROFILE + "LoginWithPassword"
        LoginWithAPIKey = PROFILE + "LoginWithAPIKey"
        RequestCaptcha = PROFILE + "RequestCaptcha"
        RequestTOTP = PROFILE + "RequestTOTP"
        UseSavedLogin = PROFILE + "UseSavedLogin"
        Forget = PROFILE + "Forget"

        class Data:
            DATA = "Profile.Data."
            Put = DATA + "Put"
            Get = DATA + "Get"

    class Search:
        SEARCH = "Search."
        Games = SEARCH + "Games"
        Users = SEARCH + "Users"

    class Fetch:
        FETCH = "Fetch."
        Game = FETCH + "Game"
        DownloadKey = FETCH + "DownloadKey"
        GameUploads = FETCH + "GameUploads"
        User = FETCH + "User"
        Sale = FETCH + "Sale"
        ProfileCollections = FETCH + "ProfileCollections"
        ProfileGames = FETCH + "ProfileGames"
        ## ProfileOwnedKeys
        # http://docs.itch.ovh/butlerd/master/#/?id=fetchprofileownedkeys-client-request
        # PARAMETERS
        # profileId     number      Profile to use to fetch game
        # limit         number      (optional) Maximum number of owned keys to return at a time.
        # search        string  	(optional) When specified only shows game titles that contain this string
        # sortBy    	string	    (optional) Criterion to sort by
        # filters   	ProfileOwnedKeysFilters	    (optional) Filters
        # reverse	    boolean	    (optional)
        # cursor	    Cursor	    (optional) Used for pagination, if specified
        # fresh	        boolean	    (optional) If set, will force fresh data
        # RESULT
        # items	        DownloadKey[]   Download keys fetched for profile
        # nextCursor	Cursor	        (optional) Used to fetch the next page
        # stale     	boolean     	(optional) If true, re-issue request with “Fresh”
        ProfileOwnedKeys = FETCH + "ProfileOwnedKeys"
        Commons = FETCH + "Commons"
        Caves = FETCH + "Caves"
        Cave = FETCH + "Cave"
        ExpireAll = FETCH + "ExpireAll"

        class Collection:
            COLLECTION = "Fetch.Collection."
            Games = COLLECTION + "Games"

    class Game:
        GAME = "Game."
        FindUploads = GAME + "FindUploads"

    class Install:
        INSTALL = "Install."
        Queue = INSTALL + "Queue"
        Plan = INSTALL + "Plan"
        Perform = INSTALL + "Perform"
        Cancel = INSTALL + "Cancel"

        class VersionSwitch:
            VERSION_SWITCH = "Install.VersionSwitch."
            Queue = VERSION_SWITCH + "Queue"

        class Locations:
            LOCATIONS = "Install.Locations."
            List = LOCATIONS + "List"
            Add = LOCATIONS + "Add"


    class Uninstall:
        UNINSTALL = "Uninstall."
        Perform = UNINSTALL + "Perform"

    class Caves:
        CAVES = "Caves."
        SetPinned = CAVES + "SetPinned"

    # Launch
    Launch = "Launch"

    class System:
        SYSTEM = "System."
        StatFS = SYSTEM + "StatFS"
